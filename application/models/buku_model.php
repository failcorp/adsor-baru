<?php
class Buku_model extends CI_Model {

	function find_all($wh = array(), $limit, $paginasi) {
		//$q= $this->db->query("
		//	select * from buku 
		//	LIMIT $page, $limit
		//");

		if (!empty($wh)) {
			$this->db->where($wh);
		}

		$this->db->from("buku");
		$this->db->limit($limit, $paginasi);
		return $this->db->get()->result_array();
	}

	public function find_by_id($id){
		$this->db->from("buku");
		$this->db->where("id",$id);
		return $this->db->get()->row_array();
	}

	public function tambah($id, $judul, $pengarang, $penerbit, $kategori){
		$data = array (
			"id"=>$id,
			"judul"=>$judul,
			"pengarang"=>$pengarang,
			"penerbit"=>$penerbit,
			"isbn"=>$isbn,
			"kategori"=>$kategori
			);

		return $this->db->insert("buku", $data);
	}

	public function update($id, $judul, $pengarang, $penerbit, $kategori) {
		$data = array(
           'judul' => $judul,
           'pengarang' => $pengarang,
           'penerbit' => $penerbit,
           //'isbn' => $isbn,
           'kategori'=>$kategori
        );//menyimpan ke data array

		$this->db->where('id', $id);//mencari berdasarkan id di database
		$this->db->update('buku', $data);

		// mengetahui jumlah data yang diupdate
		return $this->db->affected_rows(); 
	}

	public function get($wh = array()){
		if (!empty($wh)) {
			$this->db->where($wh);
		}

		$this->db->from('buku');
		return $this->db->count_all_results();
	}

	public function find_all_kategori($kategori,$limit, $paginasi) {
		$this->db->from("buku");
		$this->db->where("kategori like",$kategori.'%');
		$this->db->limit($limit, $paginasi);
		return $this->db->get()->result_array();
	}
	    
	public function count_all_by_kategori($kategori) {
		$this->db->from('buku');
		$this->db->where('kategori like', $kategori.'%');
		return $this->db->count_all_results();
	}

	public function kategori($q = "") {
		$this->db->from("buku");
		$this->db->select("kategori");
		$this->db->distinct();
		$this->db->where(array(
			"kategori like" => $q . "%"
		));
		return $this->db->get()->result_array();
	}

	public function pencarian($judul,$limit,$paginasi) {
		$this->db->from("buku");
		$this->db->where("judul like", $judul.'%');
		$this->db->limit($limit, $paginasi);
		return $this->db->get()->result_array();		
	}
	public function count_all_by_search($judul) {
		$this->db->from('buku');
		$this->db->where('judul like', $judul."%");

		return $this->db->count_all_results();
	}
	function find_urutan() {
		// //$q= $this->db->query("
		// //	select * from buku 
		// //	LIMIT $page, $limit
		// //");

		// if (!empty($wh)) {
		// 	$this->db->where($wh);
		// }

		$this->db->from("login");
		
		return $this->db->get()->result_array();
	}
	// public function find_urutan($urutan){
	// 	$this->db->from("login");
	// 	$this->db->where("urutan",$urutan);
	// 	return $this->db->get()->row_array();
	// }
	// public function pwd(){
	// 	$this->db->from("login");
	// 	$this->db->where("password",$password);
	// 	return $this->db->get()->result_array();

	// }
	

	public function cek_login($user, $password){
		$this->db->where("user", $user);
		$this->db->where("password", $password);
		$this->db->from("login");

		return $this->db->count_all_results();
	}

}

?>