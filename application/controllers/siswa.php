<?php 
class siswa extends CI_Controller{

	// public function pesan()
 //    {
 //        $this->load->library('validation');
 //        $this->load->helper('recaptcha_helper');
        
 //        $rules['username'] = 'trim|required|unique[users.username]';
 //        $rules['password'] = 'required|password|matches[password_confirm]';
 //        $rules['password_confirm'] = 'required';
 //        $rules['email'] = 'trim|required|valid_email|unique[users.email]';
 //        $rules['recaptcha_challenge_field'] = 'required|recaptcha_matches';
        
 //        $this->validation->set_rules($rules);
        
 //        $fields['username'] = 'username';
 //        $fields['password'] = 'password';
 //        $fields['password_confirm'] = 'password confirmation';
 //        $fields['email'] = 'email';
 //        $fields['recaptcha_challenge_field'] = 'answer to the security question';
        
 //        $this->validation->set_fields($fields);
        
 //        if ($this->validation->run() == FALSE)
 //        {
 //            $this->load->view('login');
 //        }
 //        else 
 //        {
 //            echo 'Success!';
 //        }
 //    } 


	public function login(){
		if($this->session->userdata("user") !== FALSE) {
			redirect(site_url("siswa/dashboard"));
		}

		$data = array(
			"error" => $this->session->flashdata('error'),
			"csrf" => $this->security->get_csrf_hash()
		);

		$this->load->view('login',$data);

	}

	public function logout() {
		$this->session->unset_userdata('user');
   session_destroy();

		if ($this->session->userdata == true) {
			redirect(site_url("siswa/login",'refresh'));
		} else {
			return false;
		}
	}
	

	public function kirim_login(){
		$this->load->model("buku_model");

		$this->load->library("form_validation");
		
		$this->form_validation->set_rules("user", "user", "trim|required|xss_clean");
		$this->form_validation->set_rules("password", "password", "trim|required|xss_clean");
		$this->form_validation->set_message('required', '%s harus diisi');

		if($this->form_validation->run() === false) {
			echo validation_errors();
		} else {
			$user = $this->input->post("user", true);
			$password = $this->input->post("password", true);

			if($this->buku_model->cek_login($user, $password) > 0) {
				$this->session->set_userdata("user", $user);
			}

			redirect(site_url("siswa/dashboard"));
		}
	}

	private $limit_page = 5;

	public function initial_pagination($config = array()) {
		$this->load->library('pagination');

		if (!isset($config["base_url"])) {
			$config['base_url'] = site_url("siswa/index") . "?page=" . $this->limit_page;
		}

		// cek dan set ketika total_rows tidak ada
		if (!isset($config["total_rows"])) {
			$config['total_rows'] = $this->buku_model->get();
		}

		$config['page_query_string'] = TRUE;

		$config['per_page'] = $this->limit_page;


		$config['first_link'] = '&lt;&lt;';
		$config['first_tag_open'] = '<li class="first">';
		$config['first_tag_close'] = '</li>';

		$config['prev_link'] = '&larr;';
		$config['prev_tag_open'] = '<li class="prev">';
		$config['prev_tag_close'] = '</li>';

		$config['cur_tag_open'] = '<li class="active"><span>';
		$config['cur_tag_close'] = '</span></li>';

		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';

		$config['next_link'] = '&rarr;';
		$config['next_tag_open'] = '<li class="next">';
		$config['next_tag_close'] = '</li>';

		$config['last_link'] = '&gt;&gt;';
		$config['last_tag_open'] = '<li class="last">';
		$config['last_tag_close'] = '</li>';

		$this->pagination->initialize($config);
	}

	public function dashboard(){
		if($this->session->userdata("user") === FALSE) {
			redirect(site_url("siswa/login"));
		}
		$this->load->view('dashboard');
		
	}

	public function index(){
		if($this->session->userdata("user") === FALSE) {
			redirect(site_url("siswa/login"));
		}

		$paginasi = $this->input->get("per_page", TRUE);
		

		$this->load->model("buku_model");//memanggil model

		$kategori = $this->input->get("kategori", TRUE);
		$judul = $this->input->get('judul', TRUE);

		if ($kategori !== FALSE || $judul !== FALSE) {
			$config = array();
			
			if ($kategori !== FALSE) {
				$config["base_url"] = site_url("siswa/index") . "?kategori=" . $kategori;
			}

			if ($judul !== FALSE) {
				$config["base_url"] = site_url("siswa/index") . "?judul=" . $judul;
			}
			$config['total_rows'] = $this->buku_model->get(array("kategori like" => $kategori  . "%", "judul like" => $judul . "%"));

			$this->initial_pagination($config);
		} else {
		
		$this->initial_pagination();
		}
	
		$data = array(
			"daftar" => $this->buku_model->find_all(array("kategori like" => $kategori  . "%", "judul like" => $judul . "%"),$this->limit_page,$paginasi),
			"kategori" => $this->buku_model->kategori(),
			"csrf" => $this->security->get_csrf_hash(),
			//"page" => $this->buku_model->get(),
			"paginasi" => $this->pagination->create_links()
			
		);

		if ($judul !== FALSE) {
			$data = array_merge($data, array("judul" => $judul));
		}

		if ($kategori !== FALSE) {
			$data = array_merge($data, array("kat" => $kategori));
		}
		
		$this->load->view("siswa_view", $data); 
}

	public function lihat($id=0){
		if($this->session->userdata("user") === FALSE) {
			redirect(site_url("siswa/login"));
		}

		$this->load->model("buku_model");

		$data = array(
			"data" => $this->buku_model->find_by_id($id)
		);

		$this->load->view("lihat", $data);
	}


	public function tambah(){//tombol untuk lihat submit
		if($this->session->userdata("user") === FALSE) {
			redirect(site_url("siswa/login"));
		}
		$this->load->model("buku_model");

		$data = array(
				"error" => $this->session->flashdata('error'),
				"judul" => $this->session->flashdata('judul'),
				"pengarang" => $this->session->flashdata('pengarang'),
				"penerbit" => $this->session->flashdata('penerbit'),
				//"isbn" => $this->session->flashdata('isbn')
		);
		$this->load->view('tambah_siswa', $data);

		// $kategori = $this->input->get("kategori", TRUE);

		// $tmp = $this->buku_model->kategori(array(
		// "kategori like" => $kategori . "%"
		// ), 0, 30, array("kategori" => "asc"));

		// $data = array();
		// foreach ($tmp as $it) {
		// array_push($data, array(
		// "kategori" => $it["kategori"]
		// ));
		// }

		// echo json_encode($data);
		
	}
	
	public function kategori(){
		$q = $this->input->get("q", TRUE);

		$this->load->model("buku_model");
		$data = $this->buku_model->kategori($q);

		echo json_encode($data);
	}
	public function submit(){//func untuk menyimpan ke database
		$this->load->model("buku_model");
		$this->load->library('form_validation');

		$config['upload_path'] = 'C:/xampp/htdocs/ci/uploads/';
		$config['allowed_types'] = 'gif|jpg|png|doc|pdf';
		$config['max_size']	= '10000';
		$config['max_width']  = '';
		$config['max_height']  = '';

		$this->load->library('upload', $config);
		if ( ! $this->upload->do_upload()){
				$error = array('error' => $this->upload->display_errors());
				$this->load->view('upload', $error);
		}else{
				$data = array('upload_data' => $this->upload->data());
		}	


		
		$this->form_validation->set_rules("id","id","trim|xss_clean");
		$this->form_validation->set_rules("judul","judul","trim|required|xss_clean");
		$this->form_validation->set_rules("pengarang","pengarang","trim|required|xss_clean");
		$this->form_validation->set_rules("penerbit","penerbit","trim|required|xss_clean");
		//$this->form_validation->set_rules("isbn","isbn","trim|required|xss_clean");
		$this->form_validation->set_message('required', '%s harus diisi');
//jika validasi error maka muncul pesan error

		if($this->form_validation->run() == false) {
			$this->session->set_flashdata('judul',$judul);
			$this->session->set_flashdata('pengarang',$pengarang);
			$this->session->set_flashdata('penerbit',$penerbit);
			//$this->session->set_flashdata('isbn',$isbn);
			$this->session->set_flashdata('error', validation_errors());
			redirect('buku/tambah');
		}
		else {//kecuali berjalan/tidak error maka jalankan syntax dibawah
			$id = $this->input->post("id",true);
			$judul = $this->input->post("judul",true);
			$pengarang = $this->input->post("pengarang",true);
			$penerbit = $this->input->post("penerbit",true);
			//$isbn = $this->input->post("isbn",true);		
			$kategori = $this->input->post("kategori",true);
			$this->buku_model->tambah($id, $judul, $pengarang, $penerbit, $kategori);
		}
		redirect(site_url('siswa'));
	}

	public function edit($id=0){
		if($this->session->userdata("user") === FALSE) {
			redirect(site_url("siswa/login"));
		}

		$this->load->model("buku_model");

		$data = array(
			"buku" => $this->buku_model->find_by_id($id)
		);

		$this->load->view("edit_buku", $data);
	}

	public function update() {
		$id = $this->input->post("id", TRUE);
		$judul = $this->input->post("judul", TRUE);
		$pengarang = $this->input->post("pengarang", TRUE);
		$penerbit = $this->input->post("penerbit", TRUE);
		//$isbn = $this->input->post("isbn", TRUE);
		$kategori = $this->input->post("kategori",true);

		$this->load->model("buku_model");

		$this->buku_model->update($id, $judul, $pengarang, $penerbit, $kategori);
		redirect(site_url('siswa'));
	}

	public function delete($id=0){
		if($this->session->userdata("user") === FALSE) {
			redirect(site_url("siswa/login"));
		}

		$this->load->model("buku_model");

		$this->db->delete('buku', array('id' => $id));
		redirect(site_url('siswa')); 
	}

	public function abs(){
		$this->load->view("absen");
	}


	public function show() {
		if($this->session->userdata("user") === FALSE) {
			redirect(site_url("siswa/login"));
		}

		$paginasi = $this->input->get("per_page", TRUE);
		
		$kategori = $this->input->post("kategori");
		$this->load->model("buku_model");
		$buku_view = $this->buku_model->find_all_kategori($kategori ,$this->limit_page,$paginasi);

		$tbody = "";
		for ($i=0; $i<count($buku_view); $i++) {
			$buku = $buku_view[$i];

			$tbody .= 
			"<tr>" . 
					"<td>" . $buku['id'] ."</td>" .
					"<td>" . $buku['judul'] ."</td>" .
					"<td>" . $buku['pengarang'] ."</td>" .
					"<td>" . $buku['penerbit'] ."</td>" .
					//"<td>" . $buku['isbn'] ."</td>" .
					"<td>" . $buku['kategori'] ."</td>" .
					"<td><a href='" . site_url("buku/lihat/" . $buku["id"]) ."'>Lihat</a></td>" .
					"<td><a href='" . site_url("buku/edit/" . $buku["id"]) ."'>Edit</a></td>" .
					"<td><a href='" . site_url("buku/delete/" . $buku["id"]) ."'>Delete</a></td>" .
			"</tr>";
		}

		echo $tbody;
	}

	public function cari() {
		if($this->session->userdata("user") === FALSE) {
			redirect(site_url("siswa/login"));
		}

		$paginasi = $this->input->get("per_page", TRUE);
		$judul = $this->input->post("judul");
		$this->load->model("buku_model");
		

		$buku_view = $this->buku_model->pencarian($judul,$this->limit_page,$paginasi);

		$tbody = "";
		for ($i=0; $i<count($buku_view); $i++) {
			$buku = $buku_view[$i];

			$tbody .= 
			"<tr>" . 
					"<td>" . $buku['id'] ."</td>" .
					"<td>" . $buku['judul'] ."</td>" .
					"<td>" . $buku['pengarang'] ."</td>" .
					"<td>" . $buku['penerbit'] ."</td>" .
					//"<td>" . $buku['isbn'] ."</td>" .
					"<td>" . $buku['kategori'] ."</td>" .
					"<td><a href='" . site_url("buku/lihat/" . $buku["id"]) ."'>Lihat</a></td>" .
					"<td><a href='" . site_url("buku/edit/" . $buku["id"]) ."'>Edit</a></td>" .
					"<td><a href='" . site_url("buku/delete/" . $buku["id"]) ."'>Delete</a></td>" .
			"</tr>";
		}

		echo $tbody;
	}
	public function show_pagination() {
		$kategori = $this->input->post("kategori");
		$this->load->model("buku_model");

		$config = array();
		$config['total_rows'] = $this->buku_model->count_all_by_kategori($kategori);
		$config["base_url"] = site_url("siswa/index") . "?kategori=" . $kategori;

		$this->initial_pagination($config);

		echo $this->pagination->create_links();
	}

	

	public function cari_pagination() {
		$cari = $this->input->post("judul");
		$this->load->model("buku_model");
		$paginasi = $this->input->get("per_page", TRUE);

		$config['per_page'] = $this->limit_page;
		$config = array();
		$config['total_rows'] = $this->buku_model->count_all_by_search($cari);
		$config["base_url"] = site_url("siswa/index") . "?judul=" . $cari;

		$this->initial_pagination($config);

		echo $this->pagination->create_links();
		}
	public function upload_ftp(){
		if($this->session->userdata("user") === FALSE) {
			redirect(site_url("siswa/login"));
		}
		
		$config['hostname'] = '127.0.0.1';
		 $config['username'] = 'guest';
		 $config['password'] = 'guest123';
		$config['port']     = 21;
		$config['passive']  = TRUE;
		$config['debug']    = TRUE;
		$config['base_folder']  = 'C:/xampp';
		

		$this->load->library('ftp');
		$this->ftp->connect($config);
		$list = $this->ftp->list_files('C:/xampp/htdocs/ci');
		$config['upload_path'] = 'C:/xampp/htdocs/ci/uploads/';
		$config['allowed_types'] = 'gif|jpg|png|doc|pdf';
		$config['max_size']	= '10000';
		$config['max_width']  = '';
		$config['max_height']  = '';

		$this->load->library('upload',$config);
		if ( ! $this->upload->do_upload()){
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('upload', $error);
			}
		else{
			$data = array('upload_data' => $this->upload->data());
			$this->load->view('upload_success');
			}
		print_r($list);

		$this->ftp->close();
//$this->ftp->upload('C:/xampp/htdocs/ci/uploads', '/public_html', 'auto', 0775);
/*if ( ! $this->upload->do_upload()){
			$error = array('error' => $this->upload->display_errors());
			$this->load->view('upload', $error);
	}else{
			$data = array('upload_data' => $this->upload->data());
	}	
$this->ftp->connect($config);*/
	}

	// public function login() {
	

	// 	$data = array(
	// 			"error" => $this->session->flashdata('error'),
	// 			"urutan" => $this->session->flashdata('urutan')
	// 			"user" => $this->session->flashdata('user'),
	// 			"password" => $this->session->flashdata('password'),
				
	// 	);
	// 	$this->load->view("login", $data);
	// }
	// public function login_user(){
		
	// 	$this->load->library('form_validation');
	// 	$this->load->buku_model();
	// 	// $cek = $this->buku_model->user_add($user,$password);
	// 	// if($cek == null || $cek == false) {
	// 	// 	redirect("siswa/login");
	// 	// 	$this->session->set_flashdata("tampilkan username atau password salah");
	// 	// } else {
	// 	// 	$this->session->set_userdata($username);
			
	// 		redirect(site_url("buku/add"));
	// 	}

	// 	public function user_baru(){
	// 		if($this->session->userdata("user") === FALSE) {
	// 			redirect(site_url("siswa/login"));
	// 		}

	// 		$data = array(
	// 			"error" => $this->session->flashdata('error'),
	// 			"userid" => $this->session->flashdata('userid'),
	// 			"user" => $this->session->flashdata('user'),
	// 			"password" => $this->session->flashdata('password'),
				
	// 	);
	// 	$this->load->view("add", $data);
	// }
// 		public function kirim(){
			
// 			$this->load->model('buku_model');
// 			$this->load->library('form_validation');
			
// 		$this->form_validation->set_rules("user","user","trim|required|xss_clean");
// 		$this->form_validation->set_rules("password","password","trim|required|xss_clean");
// 		$this->form_validation->set_message('required', '%s harus diisi');
// //jika validasi error maka muncul pesan error

// 	if($this->form_validation->run() == false) {
// 	$this->session->set_flashdata('user',$user);
// 	$this->session->set_flashdata('password',$password);
// 	$this->session->set_flashdata('error', validation_errors());
// 	redirect('buku/user_baru');
// 	}
// 	else {//kecuali berjalan/tidak error maka jalankan syntax dibawah
// 		$user = $this->input->post("user",true);
// 		$password = ($this->input->post("password",true));
		
// 		$this->buku_model->user_add($user,$password);
// 		redirect(site_url('buku/sukses'));
// 	}
// 	}
	public function sukses(){
		if($this->session->userdata("user") === FALSE) {
			redirect(site_url("siswa/login"));
		}
		
		$this->load->view('sukses');
	}
}

?>
