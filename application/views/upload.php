<html>
<head>
<title>Upload Form</title>
 <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php echo base_url("assets/css/bootstrap.css");?>" rel="stylesheet">
	<link href="<?php echo base_url("assets/css/starter-template.css");?>" rel="stylesheet">
	<script src="<?php echo base_url("assets/js/jquery-1.11.1.min.js");?>"></script>
	<script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	  <div class="container">
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="#">ADSOR Jemursari</a>
	    </div>
	    <div class="collapse navbar-collapse">
	      <ul class="nav navbar-nav">
	        <li><a href="<?=site_url("siswa/dashboard")?>">Dashboard</a></li>
	        <li><a href="<?=site_url("siswa/tambah")?>">Tambah Siswa</a></li>
	        <li class="active"><a href="<?=site_url("siswa/upload_ftp")?>"> Upload Bahan </a></li>
	        <!-- <li><a href="<?=site_url("buku/user_baru")?>"> tambah user </a></li> -->
	      </ul>
	      <ul class="navbar navbar-nav navbar-right">
	      	<li><a href="<?=site_url("siswa/logout")?>">Logout</a></li>
	      </ul>
	    </div>
	  </div>
	</div>

<br/>
<?php echo form_open_multipart('siswa/upload_ftp');?>

<input type="file" name="userfile" size="20" />

<br /><br />

<input type="submit" class="btn btn-success" value="upload" /> 
<input type="button" value="back" onclick="window.history.back()" class="btn btn-primary">
</form>
	

<script src="<?php echo base_url("assets/js/jquery-1.11.1.min.js");?>"></script>
<script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>
</body>
</html>