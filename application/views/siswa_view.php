<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<?php echo base_url("assets/css/bootstrap.css");?>" rel="stylesheet">
<link href="<?php echo base_url("assets/css/starter-template.css");?>" rel="stylesheet">
<script src="<?php echo base_url("assets/js/jquery-1.11.1.min.js");?>"></script>
<script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>
	<title>Daftar Siswa</title>
</head>
<body>
	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	  <div class="container">

	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?=site_url("siswa/dashboard")?>">ADSOR Jemursari</a>
	    </div>
	    <div class="collapse navbar-collapse">
	      <ul class="nav navbar-nav">
	        <li><a href="<?=site_url("siswa/dashboard")?>">Dashboard</a></li>
	        <li class="dropdown">
          	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Siswa <span class="caret"></span></a>
          		<ul class="dropdown-menu" role="menu">
		            <li><a href="<?=site_url("siswa/tambah")?>">Tambah Siswa</a></li>
		            <li><a href="<?=site_url("siswa")?>">Daftar siswa</a></li>
		            <li><a href="<?=site_url("siswa/abs")?>">Absensi</a></li>
		        </ul>
		    </li>
	        <li><a href="<?=site_url("siswa/upload_ftp")?>"> Upload Bahan </a></li>
	        <!-- <li><a href="<?=site_url("siswa/user_baru")?>"> tambah user </a></li> -->
	      </ul>
	      <ul class="navbar navbar-nav navbar-right">
	      	<li><a href="<?=site_url("siswa/logout")?>">Logout</a></li>
	      </ul>
	    </div>
	  </div>
	</div>
<BR/>

	<div class="container">
	<div class="row">
	<h1>Daftar Siswa</h1>
	<div class="col-md-4">
	<h3>pilih berdasar kelas:</h3></div>
	<div class="col-md-4 col-md-offset-4">
	<h3>cari nama siswa:</h3></div>
		<div class="col-md-4"> 
		<select id="kategori">
		<option value="">semua</option>
		<?php
			for ($i=0; $i < count($kategori) ; $i++) {
				$c = $kategori[$i]; 
		?>

			<option><?=$c["kategori"]?></option>
		<?php
			}
		?>
		</select></div>
 	<div class="col-md-4 col-md-offset-4">
 		<input type="textbox" id="sbox" class="text"/>
 		
 <br/>
 
</div>
</div></div></div></div>
		</select>
		
		<br/><br/>
<div class="container">

		<table class="table table-striped table-bordered">
		
			<thead>
				<tr>
					<th style="min-width: 30px">ID</th>
					<th style="min-width: 150px;">Nama</th>
					<th style="min-width: 100px;">Alamat</th>
					<th style="min-width: 150px;">Jurusan</th>
					<th style="min-width: 150px;">Kelas</th>
					<th style="min-width: 50px;">lihat</th>
					<th style="min-width: 50x;">edit</th>
					<th style="min-width: 50px;">hapus</th>
				</tr>
			</thead>
			</div>
			<tbody>
<?php
		for ($i=0; $i <count($daftar) ; $i++) {
		$r = $daftar[$i]; 
	?>			
				<tr>
					<td><?=$r["id"]?></td>
					<td><?=$r["judul"]?></td>
					<td><?=$r["pengarang"]?></td>
					<td><?=$r["penerbit"]?></td>
					<td><?=$r["kategori"]?></td>
					<td><a href="<?php echo site_url("siswa/lihat/" . $r["id"]);?>">Lihat</a></td>
					<td><a href="<?php echo site_url("siswa/edit/" . $r["id"]);?>">edit</a></td>
					<td><a href="<?php echo site_url("siswa/delete/" . $r["id"]);?>">Delete</a></td>

	</tr>

	<?php
	}
	?>
			</tbody>
			
	
		</table>

		<ul class="pagination"><?= $paginasi ?></ul>

	

	 <script>
	// jQuery(document).ready(function($){

	// 	    var tbody = $("table > tbody");

	// 		$("#kategori").change(function(e) {
	// 			$.post("<?=site_url("siswa/show")?>", {kategori:$(this).val(), csrf_1234:"<?=$csrf?>"}, function(resp) {
	// 				tbody.html(resp);
	// 			});
	// 		});


	// 	});	
	// jQuery(document).ready(function($){

	// 	    var tbody = $("table > tbody");

	// 		$("#sbox").change(function(e) {
	// 			$.post("<?=site_url("siswa/cari")?>", {judul:$(this).val(), csrf_1234:"<?=$csrf?>"}, function(resp) {
	// 				tbody.html(resp);
	// 			});
	// 		});

			
	// 	});		
	//
	jQuery.noConflict();
		jQuery(document).ready(function($){

			$('#kategori').change(function(){
				$.post( 
				'<?=site_url("siswa/show")?>',//memanggil controlernya
				{kategori:$("#kategori").val(), csrf_1234:"<?=$csrf?>"},//mendapatkan nilai
				function(resp){
					$("table > tbody").html(resp);
				});

				$.post(
					'<?=site_url("siswa/show_pagination")?>',//memanggil controlernya
					{kategori:$("#kategori").val(), csrf_1234:"<?=$csrf?>"},
					function(resp) {
						$("ul[class=pagination]").html(resp);
					}
				);
			});
				
		 	$('#sbox').keyup(function(){
				$.post('<?=site_url("siswa/cari")?>',
					{judul :$(this).val(), csrf_1234:"<?=$csrf?>"},
					function(data){
						$("table > tbody").html(data);

					}
				);

				$.post(
					'<?=site_url("siswa/cari_pagination")?>',//memanggil controlernya
					{judul:$(this).val(), csrf_1234:"<?=$csrf?>"},
					function(resp) {
						$("ul[class=pagination]").html(resp);
					}
				);
			});

		 	<?php
		 		if (isset($judul)) {
 			?>
 				$("#sbox").val("<?=$judul?>");
 			<?php
		 		}
		 	?>
		 	
		 	<?php
		 		if (isset($kat)) {
 			?>
 				$("#kategori").val("<?=$kat?>");
 			<?php
		 		}
		 	?>

		});
	 </script>

</body>
</html>
