<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<?php echo base_url("assets/css/bootstrap.css");?>" rel="stylesheet">
<link href="<?php echo base_url("assets/css/starter-template.css");?>" rel="stylesheet">
<script src="<?php echo base_url("assets/js/jquery-1.11.1.min.js");?>"></script>
<script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>
	<title>Dashboard</title>
</head>
<body>

<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	<div class="container">

	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?=site_url("siswa/dashboard")?>">ADSOR Jemursari</a>
	    </div>
	    <div class="collapse navbar-collapse">
	      <ul class="nav navbar-nav">
	        <li class="active"><a href="<?=site_url("siswa/dashboard")?>">Dashboard</a></li>
	        <li class="dropdown">
	      	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Siswa <span class="caret"></span></a>
	      		<ul class="dropdown-menu" role="menu">
		            <li><a href="<?=site_url("siswa/tambah")?>">Tambah Siswa</a></li>
		            <li><a href="<?=site_url("siswa")?>">Daftar siswa</a></li>
		            <li><a href="<?=site_url("siswa/abs")?>">Absensi</a></li>
		        </ul>
		    </li>
	        <li><a href="<?=site_url("siswa/upload_ftp")?>"> Upload Bahan </a></li>
	        <!-- <li><a href="<?=site_url("siswa/user_baru")?>"> tambah user </a></li> -->
	      </ul>
	      <ul class="navbar navbar-nav navbar-right">
	      	<li><a href="<?=site_url("siswa/logout")?>">Logout</a></li>
	      </ul>
	    </div>
	</div>
</div>

<BR/>

<div class="container">
<h2>selamat datang</h2>	

</div>
</body>
</html>