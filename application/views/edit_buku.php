<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link href="<?php echo base_url("assets/css/bootstrap.css");?>" rel="stylesheet">
<link href="<?php echo base_url("assets/css/starter-template.css");?>" rel="stylesheet">

	<title>Edit</title>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	  <div class="container">

	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?=site_url("siswa/dashboard")?>">ADSOR Jemursari</a>
	    </div>
	    <div class="collapse navbar-collapse">
	      <ul class="nav navbar-nav">
	        <li><a href="<?=site_url("siswa/dashboard")?>">Dashboard</a></li>
	        <li class="dropdown">
          	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Siswa <span class="caret"></span></a>
          		<ul class="dropdown-menu" role="menu">
		            <li><a href="<?=site_url("siswa/tambah")?>">Tambah Siswa</a></li>
		            <li><a href="<?=site_url("siswa")?>">Daftar siswa</a></li>
		            <li><a href="<?=site_url("siswa/abs")?>">Absensi</a></li>
		        </ul>
		    </li>
	        <li><a href="<?=site_url("siswa/upload_ftp")?>"> Upload Bahan </a></li>
	        <!-- <li><a href="<?=site_url("siswa/user_baru")?>"> tambah user </a></li> -->
	      </ul>
	      <ul class="navbar navbar-nav navbar-right">
	      	<li><a href="<?=site_url("siswa/logout")?>">Logout</a></li>
	      </ul>
	    </div>
	  </div>
	</div>

	<BR/>
	<div class="container">
	<h1>Edit/Update buku:</h1>
		<?=form_open('siswa/update',array('method' => 'post' , 'role' => 'form'))?>
			<div class="row">
			<div class="col-md-2 form-group">
				<p>ID    :</p><input name="id" type="int" class="form-control" value="<?=$buku["id"]?>"/>
			</div>
			<div class="col-md-4 col-md-offset-4 form-group">
				<p>Nama    :</p><input name="judul" type="text" class="form-control" value="<?=$buku["judul"]?>" />
			</div>
			<div class="col-md-3 form-group">
				<p>Alamat :</p><input name="pengarang" type="text" class="form-control" value="<?=$buku["pengarang"]?>"/>
			</div>
			<div class="col-md-4 col-md-offset-3 form-group">
				<p>Jurusan   :</p><input name="penerbit" type="text" class="form-control" value="<?=$buku["penerbit"]?>"/>
			</div>
	<div class="col-md-5 form-group">
					Kelas		: 
					<select name="kategori">
						
						<option>Kelas Akselerasi</option>
						<option>Kelas reguler</option>
					</select> <br/>
				</div>
	</div>
		<BR/>
			<input type="button" value="back" onclick="window.history.back()" class="btn btn-primary">
				
			</form>	
			
		
				

<script src="<?php echo base_url("assets/js/jquery-1.11.1.min.js");?>"></script>
<script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>	
</body>
</html>