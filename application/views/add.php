<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php echo base_url("assets/css/bootstrap.css");?>" rel="stylesheet">
	<link href="<?php echo base_url("assets/css/starter-template.css");?>" rel="stylesheet">
	<script src="<?php echo base_url("assets/js/jquery-1.11.1.min.js");?>"></script>
	<script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>
	<script src="<?php echo base_url("assets/js/jquery.validate.js");?>"></script>
	<title>Menambah buku</title>
</head>

<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	  <div class="container">

	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?=site_url("siswa/dashboard")?>">ADSOR Jemursari</a>
	    </div>
	    <div class="collapse navbar-collapse">
	      <ul class="nav navbar-nav">
	        <li><a href="<?=site_url("siswa/dashboard")?>">Dashboard</a></li>
	        <li class="dropdown">
          	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Siswa <span class="caret"></span></a>
          		<ul class="dropdown-menu" role="menu">
		            <li><a href="<?=site_url("siswa/tambah")?>">Tambah Siswa</a></li>
		            <li><a href="<?=site_url("siswa")?>">Daftar siswa</a></li>
		            <li><a href="#">Absensi</a></li>
		        </ul>
		    </li>
	        <li><a href="<?=site_url("siswa/upload_ftp")?>"> Upload Bahan </a></li>
	        <!-- <li><a href="<?=site_url("siswa/user_baru")?>"> tambah user </a></li> -->
	      </ul>
	      <ul class="navbar navbar-nav navbar-right">
	      	<li><a href="<?=site_url("siswa/logout")?>">Logout</a></li>
	      </ul>
	    </div>
	  </div>
	</div>
	<BR/>

<div class="container">
<?php
	if($error) {
?>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
			<div class="alert alert-danger">
				<?=$error?>
		</div>
		</div>
		</div>
<?php
	}
?>


	<div class="container">
	<h1>Tambah user</h1>
		<?=form_open("buku/kirim")?>	
		
			<div class="row">
				<div class="col-md-3 form-group">
					user	        :<input name="user" type="text" class="form-control" /> <br/>
				</div>
				<div class="col-md-3 col-md-offset-2 form-group">
					password        :<input name="password" type="password" class="form-control" /> 
				</div>
			</div>
			</div>
			
			<br/>
			<br/>
			
			<input type="submit" value="add" class="btn btn-success" /> 
			<a href="http://localhost/ci/index.php/buku" class="btn btn-info">back</a>
		<!--</form>-->
		<?=form_close()?>

<script>
	jQuery(document).ready(function($){
		var validasi = $('#validasi');

		validasi.validate({
			rules : {
				user:{
					required:true
				},
				password:{
					required:true
				},
				
			},
			messages: {
				user: {
					required:"masukkan user"
				},
				password: {
					required:"masukkan password maksimal 25 karakter"
				},
				
			}
		});
	});
</script>
</body>
</html>