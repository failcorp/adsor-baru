<!DOCTYPE html>
<html>
<head>

<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php echo base_url("assets/css/bootstrap.css");?>" rel="stylesheet">
	<link href="<?php echo base_url("assets/css/starter-template.css");?>" rel="stylesheet">
	<script src="<?php echo base_url("assets/js/jquery-1.11.1.min.js");?>"></script>
	<script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>
	<script src="<?php echo base_url("assets/js/jquery.validate.js");?>"></script>
	<script src="<?php echo base_url("assets/js/typeahead.js");?>"></script>
	<title>Menambah Siswa</title>
</head>

<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	  <div class="container">

	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a class="navbar-brand" href="<?=site_url("siswa/dashboard")?>">ADSOR Jemursari</a>
	    </div>
	    <div class="collapse navbar-collapse">
	      <ul class="nav navbar-nav">
	        <li><a href="<?=site_url("siswa/dashboard")?>">Dashboard</a></li>
	        <li class="dropdown">
          	<a href="#" class="dropdown-toggle" data-toggle="dropdown">Siswa <span class="caret"></span></a>
          		<ul class="dropdown-menu" role="menu">
		            <li><a href="<?=site_url("siswa/tambah")?>">Tambah Siswa</a></li>
		            <li><a href="<?=site_url("siswa")?>">Daftar siswa</a></li>
		            <li><a href="<?=site_url("siswa/abs")?>">Absensi</a></li>
		        </ul>
		    </li>
	        <li><a href="<?=site_url("siswa/upload_ftp")?>"> Upload Bahan </a></li>
	        <!-- <li><a href="<?=site_url("siswa/user_baru")?>"> tambah user </a></li> -->
	      </ul>
	      <ul class="navbar navbar-nav navbar-right">
	      	<li><a href="<?=site_url("siswa/logout")?>">Logout</a></li>
	      </ul>
	    </div>
	  </div>
	</div>
	<BR/>

<div class="container">
<?php
	if($error) {
?>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
			<div class="alert alert-danger">
				<?=$error?>
		</div>
		</div>
		</div>
<?php
	}
?>


	<div class="container">
	<h1>Tambah Siswa</h1>
		<?=form_open_multipart("siswa/submit", array("id" => "validasi","method" => "POST","role" => "form"))?>	
		<!--<form id='validasi' action="<?= site_url('siswa/submit'); ?>" method="post" role="form">-->
			<div class="row">
				<div class="col-md-2 form-group">
					ID	        :<input name="id" type="text" class="form-control"> <br/>
				</div>
				<div class="col-md-4 col-md-offset-4 form-group">
					Nama        :<input name="judul" type="text" class="form-control" value="<?=$judul?>"> <br/>
				</div>	
				<div class="col-md-3 form-group">
					Alamat	:<input name="pengarang" type="text" class="form-control" value="<?=$pengarang?>"> <br/>
				</div>
				<div class="col-md-4 col-md-offset-3 form-group">
					Jurusan	:<input name="penerbit" type="text" class="form-control" value="<?=$penerbit?>"> <br/>
				</div>
				<div class="col-md-5  form-group">
				
					Kelas		: 
	<div id="Bloodhound">
  <input name="kategori" class="typeahead" type="text" placeholder="kategori">
</div>
					

					<br/>
				</div>
			</div>
			<br/>
			<input type="file" name="userfile" size="20" />
			<br/>
			<input type="submit" value="submit" class="btn btn-success" />
			<input type="button" value="back" onclick="window.history.back()" class="btn btn-primary">
		<!--</form>-->
		<?=form_close()?>
<script>
	jQuery(document).ready(function($){
		var frm = $("form");

		var kategori = new Bloodhound({
			remote: "<?= site_url('siswa/kategori') ?>?q=%QUERY",
			datumTokenizer: function(d){
				return Bloodhound.tokenizers.whitespace(d.val);
			},
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			limit: 5
		});

		kategori.initialize();

		frm.find("input[name=kategori]").typeahead({
			minLength: 1,
			highlight: true
		},{
			displayKey: "kategori",
			source: kategori.ttAdapter()
		});
		
	});
		
		
			</script>

<script>
	jQuery(document).ready(function($){
		var validasi = $('#validasi');

		validasi.validate({
			rules : {
				judul:{
					required:true
				},
				pengarang:{
					required:true
				},
				penerbit:{
					required:true
				},
				isbn:{
					required:true
				},
			},
			messages: {
				judul: {
					required:"masukkan nama siswa"
				},
				pengarang: {
					required:"masukkan alamat siswa"
				},
				penerbit: {
				required:"masukkan jurusan siswa"
							},
				// isbn: {
				// required:"masukkan nomer isbn"
							// },
			}
		});
	});
</script>
</body>
</html>