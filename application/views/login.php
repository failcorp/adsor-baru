<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="<?php echo base_url("assets/css/bootstrap.css");?>" rel="stylesheet">
	<link href="<?php echo base_url("assets/css/starter-template.css");?>" rel="stylesheet">
	<script src="<?php echo base_url("assets/js/jquery-1.11.1.min.js");?>"></script>
	<script src="<?php echo base_url("assets/js/bootstrap.min.js");?>"></script>
	<script src="<?php echo base_url("assets/js/jquery.validate.js");?>"></script>
	<title>Welcome...to ADSOR Jemursari</title>
</head>

<body>
<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
	  <div class="container">

	    <div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Navigasi</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">ADSOR Jemursari</a>
	    </div>
	    
	  </div>
	</div>
	<BR/>

<div class="container">
<?php
	if($error) {
?>
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
			<div class="alert alert-danger">
				<?=$error?>
		</div>
		</div>
		</div>
<?php
	}
?>


	<div class="container">
	<h1>silahkan login...</h1>
		<?= form_open("siswa/kirim_login") ?>	
		
			<div class="row">
				<div class="col-md-3 form-group">
					user	        :<input name="user" id="user" type="text" class="form-control" /> <br/>
				</div>
				<div class="col-md-3 col-md-offset-1 form-group">
					password        :<input name="password" id="password" type="password" class="form-control" /> 
				</div>
			</div>
			<input type="submit" value="login" class="btn btn-success" />
			</div>	
			
		<!--</form>-->
		<?= form_close() ?>

</body>
</html>